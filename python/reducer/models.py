from typing import Dict, List, Tuple
from time import time

class Reducer:
    def __init__(self: object) -> None:
        self.__records: Dict[str, Dict[str, int]] = {}

    def __dump_file(self: object, name: str, data: List[str]) -> None:
        open(name.split(".")[0] + "-REDUCED.txt", "w+").writelines(data)

    def __get_file(self: object, name: str) -> List[str]:
        return open(name, "r").readlines()

    def __remove_duplicates(self: object, data: List[str]) -> Tuple[List[str],
                                                                    Dict[str, int]]:
        dupe: Dict[str, int] = {}; seen: List[str] = []
        for i in data:
            if i not in seen:
                seen.append(i)
            dupe[i] = 0 if i not in dupe.keys() else dupe[i] = dupe[i] + 1
        return seen, dupe

    def get_records(self: object, name: str = "") -> int:
        return self.__records if name == "" else self.__records[name]

    def reduce(self: object, name: str, dump: bool) -> Tuple[List[str], Dict[str, int]]:
        result: Tuple[List[str],
                      Dict[str, int]] = self.__remove_duplicates(self.__get_file(name))
        self.__records.update({(
                name
                if name not in self.__records.keys() else
                self.__records[name + str(time())]
            ), result[1]
        })
        if dump: self.__dump_file(name, result[0])
        return result