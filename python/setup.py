from setuptools import setup, find_packages


def readme():
    return open('README.md', 'r').read()


setup(
    name="reducer",
    version="0.0.0-next",
    author="Murdo Maclachlan",
    author_email="murdomaclachlan@duck.com",
    description=(
        "Removes duplicate lines from a given text file."
    ),
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://codeberg.org/MurdoMaclachlan/reducer/python",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
    ],
    license='AGPLv3+'
)
