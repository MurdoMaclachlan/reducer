package main.java;

/**
 * A simple, immutable data structure containing two variables of unspecified types,
 * which can be the same type or different types. Tuples are used to easily package
 * data of various types to be returned from the same function.
 *
 * @param <K> The type of the first item.
 * @param <V> The type of the second item.
 *
 * @author Murdo B. Maclachlan
 * @version 1.0.0
 */
public class Tuple<K, V> {
    private final K itemOne;
    private final V itemTwo;

    /**
     * Constructor for the Tuple class.
     *
     * @param itemOne The first item in the Tuple.
     * @param itemTwo The second item in the Tuple.
     *
     * @since 1.0.0
     */
    Tuple(K itemOne, V itemTwo) {
        this.itemOne = itemOne;
        this.itemTwo = itemTwo;
    }

    /**
     * Getter for the first item in the Tuple.
     *
     * @return The first item in the Tuple.
     *
     * @since 1.0.0
     */
    public K getFirst() {
        return this.itemOne;
    }

    /**
     * Getter for the second item in the Tuple.
     *
     * @return The second item in the Tuple.
     *
     * @since 1.0.0
     */
    public V getSecond() {
        return this.itemTwo;
    }
}
