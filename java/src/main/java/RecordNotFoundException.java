package main.java;

/**
 * @author Murdo B. Maclachlan
 * @since 1.0.0
 */
public class RecordNotFoundException extends Exception {
    private final String call;
    private final String name;

    public RecordNotFoundException(String call, String name) {
        this.call = call;
        this.name = name;
    }

    public String getCall() {
        return this.call;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.call + " called, but no record matching the given name, '" + this.name + "', was found.";
    }

    public boolean equals(Object other) {
        if (other instanceof RecordNotFoundException)
            return this.call.equals(((RecordNotFoundException)other).getCall());
        return false;
    }
}