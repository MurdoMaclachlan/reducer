package main.java;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Utility class for reading in data from text files, and removing any duplicate lines
 * in the data, recording what files have been reduced, and how many duplicates each line
 * in those files had.
 *
 * @author Murdo B. Maclachlan
 * @version 1.0.0
 */
public class Reducer {
    private final HashMap<String, HashMap<String, Integer>> records;

    /**
     * Constructor for the Reducer class; initialises the records attribute.
     *
     * @since 1.0.0
     */
    public Reducer() {
        this.records = new HashMap<>();
    }

    public static void main(String[] args) {
        Reducer reducer = new Reducer();
    }

    /**
     * Outputs reduced data to a new text file, of file name format:
     * "[original_name]-REDUCED.txt"
     *
     * @param name The full or relative path of the original file.
     * @param data The data to output.
     * @throws IOException If the output file path cannot be accessed.
     *
     * @since 1.0.0
     */
    private void dumpData(String name, ArrayList<String> data) throws IOException {
        BufferedWriter dumpFile = new BufferedWriter(new FileWriter(name.split("\\.")[0] + "-REDUCED.txt"));
        for (String line : data) {
            dumpFile.write(line);
            dumpFile.newLine();
        }
    }

    /**
     * Get the current date and time in a human-readable format.
     *
     * @return As a String, the current datetime in the format yyyy-MM-dd HH:mm:ss.
     *
     * @since 1.0.0
     */
    private String getDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    /**
     * Reads in unreduced data from a given text file.
     *
     * @param name The full or relative path of the source file.
     * @return The data, in the form of an ArrayList, with one entry per line.
     *
     * @throws FileNotFoundException If the source file cannot be found.
     */
    private ArrayList<String> getFile(String name) throws FileNotFoundException {
        ArrayList<String> data = new ArrayList<>();
        Scanner sourceFile = new Scanner(new File(name));
        while (sourceFile.hasNextLine()) {
            data.add(sourceFile.nextLine());
        }
        return data;
    }

    /**
     * Getter for the entire records HashMap.
     *
     * @return A HashMap mapping String keys to HashMap values.
     *
     * @since 1.0.0
     */
    public HashMap<String, HashMap<String, Integer>> getRecords() {
        return this.records;
    }

    /**
     * Getter for a specific value from the records HashMap.
     *
     * @param name The key of which to retrieve the corresponding value.
     * @return The value corresponding to the given key.
     * @throws RecordNotFoundException If the given key does not exist within the records HashMap.
     *
     * @since 1.0.0
     */
    public HashMap<String, Integer> getRecords(String name) throws RecordNotFoundException {
        if (this.records.containsKey(name))
            return this.records.get(name);
        else throw new RecordNotFoundException(".getRecords()", name);
    }

    /**
     * Top-level method to run through reduction process: removes duplicates, updates records, dumps reduced data into a
     * text file if requested, and returns the result.
     *
     * @param name The name of the file to reduce.
     * @param dump If the reduced data should be dumped into a text file.
     * @return The reduced data.
     * @throws IOException If any errors arise from accessing or creating files.
     *
     * @since 1.0.0
     */
    public Tuple<ArrayList<String>, HashMap<String, Integer>> reduce(String name, boolean dump)
            throws IOException {
        Tuple<ArrayList<String>, HashMap<String, Integer>> result = removeDuplicates(getFile(name));
        this.records.put(this.records.containsKey(name) ? name + getDateTime() : name, result.getSecond());
        if (dump)
            dumpData(name, result.getFirst());
        return result;
    }

    /**
     * Iterates through the given data and removes any duplicated lines, recording how many duplicates
     * a given line had.
     *
     * @param data The list of lines to remove duplicates from.
     * @return The reduced data, in the form of a Tuple.
     *
     * @since 1.0.0
     */
    private Tuple<ArrayList<String>, HashMap<String, Integer>> removeDuplicates(ArrayList<String> data) {
        HashMap<String, Integer> dupe = new HashMap<>();
        ArrayList<String> seen = new ArrayList<>();
        for (String line : data) {
            if (!seen.contains(line)) {
                seen.add(line);
            }
            dupe.put(line, dupe.containsKey(line) ? dupe.get(line) + 1 : 0);
        }
        return new Tuple<>(seen, dupe);
    }
}